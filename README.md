[![pipeline status](https://gitlab.com/azae/games/six-hats/badges/master/pipeline.svg)](https://gitlab.com/azae/games/six-hats/commits/master)

# Six hats

[Cards](https://gitlab.com/azae/games/six-hats/builds/artifacts/master/browse/_output?job=squib).

## Build

    docker run --rm --user `id -u`:`id -g` -it -v $(pwd):/data -w /data registry.gitlab.com/azae/outils/squib:0.14.2 make
